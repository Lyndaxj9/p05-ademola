Omowumi Lynda Ademola

CS441 Mobile Game Design

Program 05

/==============================================/

Very basic free project that is skeleton code for Program 06
On the battle page buttons 0, 1 and 4 have skill attached to them however 2 and 3 do not.

/==============================================/

commit 3caade2d1962ddeb41da45ac89b8fa593dee831c
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Tue Apr 4 00:28:44 2017 -0400

    made attack more visible in NSLog

commit 252ddc7980f6be09967c3b87a2e9adbc5560d8a6
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Fri Mar 31 01:28:46 2017 -0400

    have enemy calling attack regularly

commit fd9427d2cb642711bb85928286022b40f1898b5b
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Fri Mar 31 01:26:21 2017 -0400

    working on combat system

commit fb0f20a848d0810f2369e9f6726361aa86ad9ada
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Mar 30 01:21:13 2017 -0400

    cleaning up scenes

commit 6f4139832c12c9750d302a9ecdc48a3d01340892
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Thu Mar 30 00:24:19 2017 -0400

    figuring out battle scene and health bar

commit e2c7ab398649a3f5d9fb5d82bf9f642717962b16
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Wed Mar 29 03:45:13 2017 -0400

    animation and added scene

commit bc18ba64c00eed9dbe61c793aa710ad4ffb9b697
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Mar 27 22:52:17 2017 -0400

    added more scenes and buttons

commit 46d011dd7ee7d570cde938b721c73e650b347f3c
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Mon Mar 27 00:19:46 2017 -0400

    adding sklabelnode

commit 8e67f81631e592afbeac1e71f9aa2efd8c611651
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Sun Mar 26 23:18:06 2017 -0400

    Setting up scenes

commit 3541171b6bbb5d6ff0707aaefbb42a6bc26d77a9
Author: Lynda <Lynda@Mac-Users-MacBook-6.local>
Date:   Fri Mar 24 00:46:16 2017 -0400

    parsing spritesheet
