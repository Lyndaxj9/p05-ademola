//
//  SpriteSheet.h
//  p05-ademola
//
//  Created by Lynda on 3/21/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface SpriteSheet : SKTexture
@property (nonatomic, strong) SKTexture *texture;
@property (nonatomic)         NSInteger rows;
@property (nonatomic)         NSInteger cols;
@property (nonatomic)         CGFloat   margin;
@property (nonatomic)         CGFloat   spacing;
@property (nonatomic, getter=frameSize)         CGSize    frameSize;

- (instancetype)initWithTextureName:(NSString *)name rows:(NSInteger)rows cols:(NSInteger)cols margin:(CGFloat)margin spacing:(CGFloat)spacing;
- (SKTexture *)textureForColumn:(NSInteger)column andRow:(NSInteger)row;
@end
