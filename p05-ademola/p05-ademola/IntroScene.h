//
//  IntroScene.h
//  p05-ademola
//
//  Created by Lynda on 3/26/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "IntroN.h"
#import "CharCScene.h"

@interface IntroScene : SKScene
@property (nonatomic) IntroN *introN;
@end
