//
//  CharCScene.h
//  p05-ademola
//
//  Created by Lynda on 3/27/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "CharCNode.h"
#import "BattleScene.h"

@interface CharCScene : SKScene
@property (nonatomic) CharCNode *CharCN;
@property (nonatomic) UITextField *nameField;
@end
