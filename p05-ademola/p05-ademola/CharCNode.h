//
//  CharCNode.h
//  p05-ademola
//
//  Created by Lynda on 3/27/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "SpriteSheet.h"

@interface CharCNode : SKNode
@property (nonatomic) SKSpriteNode *buttonNext;
@property (nonatomic) SKSpriteNode *character;
@property (nonatomic) CGFloat width, height;

- (id)initWithSize:(CGSize)frame;
- (UITextField *)createTextField;
@end
