//
//  MyScene.h
//  p05-ademola
//
//  Created by Lynda on 3/21/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "IntroScene.h"

@interface MyScene : SKScene
@property (nonatomic) SKSpriteNode * player;
@end
