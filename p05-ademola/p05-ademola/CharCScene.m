//
//  CharCScene.m
//  p05-ademola
//
//  Created by Lynda on 3/27/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import "CharCScene.h"

@implementation CharCScene

- (void)didMoveToView:(SKView *)view
{
    [self createSceneContents];
}

- (void)createSceneContents
{
    _CharCN = [[CharCNode alloc]initWithSize:self.size];
    [self addChild:_CharCN];
    
    _nameField = [_CharCN createTextField];
    
    [self.view addSubview:_nameField];
}

- (void)createBattleScene
{
    NSLog(@"to character creation screen");
    
    [_nameField removeFromSuperview];
    
    SKScene *battleS = [[BattleScene alloc]initWithSize:self.size];
    SKTransition *doors = [SKTransition doorsOpenVerticalWithDuration:0.5];
    [self.view presentScene:battleS transition:doors];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if([node.name isEqualToString:@"nextbutton"]) {
        [self createBattleScene];
    }
}
@end
