//
//  MyScene.m
//  p05-ademola
//
//  Created by Lynda on 3/21/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import "MyScene.h"
#import "SpriteSheet.h"

@implementation MyScene

- (id)initWithSize:(CGSize)size {
    self = [super initWithSize:size];
    
    if(self){
        self.backgroundColor = [SKColor whiteColor];
        SKShapeNode *box = [SKShapeNode shapeNodeWithRectOfSize:CGSizeMake(self.size.width-40, self.size.height-100)];
        box.position = CGPointMake(20+box.frame.size.width/2, 80+box.frame.size.height/2);
        box.fillColor = [SKColor grayColor];
        [self addChild:box];

        /*
        SpriteSheet *SS = [[SpriteSheet alloc] initWithTextureName:@"skelly" rows:4 cols:9 margin:15 spacing:15];
        _player = [SKSpriteNode spriteNodeWithTexture:[SS textureForColumn:0 andRow:2]];
        self.player.position = CGPointMake(100, 100);
        [self addChild:self.player];
         */
        
        //Game Title
        SKLabelNode *title = [SKLabelNode labelNodeWithFontNamed:@"Baskerville"];
        title.text = @"THE GAME";
        title.name = @"title";
        //multilineLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
        title.fontSize = 60;
        title.fontColor = [SKColor whiteColor];
        title.position = CGPointMake(self.size.width/2, self.size.height/2);
        title.zPosition = 2;
        [self addChild:title];

        
        //creating a button
        //http://stackoverflow.com/questions/19082202/setting-up-buttons-in-skscene
        SKSpriteNode *start = [SKSpriteNode spriteNodeWithImageNamed:@"red-button-hi"];
        start.position = CGPointMake(330, 80);
        start.name = @"startbutton";
        start.zPosition = 1.0;
        start.xScale = .2;
        start.yScale = .2;
        [self addChild:start];
    }
    
    return self;
}

- (void) startGame
{
    SKScene *intro = [[IntroScene alloc]initWithSize:self.size];
    SKTransition *doors = [SKTransition doorsOpenVerticalWithDuration:0.5];
    [self.view presentScene:intro transition:doors];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if([node.name isEqualToString:@"startbutton"]) {
        [self startGame];
    }
}
@end
