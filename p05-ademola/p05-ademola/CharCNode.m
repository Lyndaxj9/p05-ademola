//
//  CharCNode.m
//  p05-ademola
//
//  Created by Lynda on 3/27/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import "CharCNode.h"

@implementation CharCNode

- (id)initWithSize:(CGSize)frame
{
    self = [super init];
    
    if(self){
        _width = frame.width;
        _height = frame.height;
        
        //init button
        //creating a button
        //http://stackoverflow.com/questions/19082202/setting-up-buttons-in-skscene
        _buttonNext = [SKSpriteNode spriteNodeWithImageNamed:@"red-button-hi"];
        _buttonNext.position = CGPointMake(330, 70);
        _buttonNext.name = @"nextbutton";
        _buttonNext.zPosition = 1.0;
        _buttonNext.xScale = .2;
        _buttonNext.yScale = .2;
        [self addChild:_buttonNext];
        
        SKTextureAtlas *walkl = [SKTextureAtlas atlasNamed:@"walkleft"];
        SKTexture *wl1 = [walkl textureNamed:@"walk-left01"];
        SKTexture *wl2 = [walkl textureNamed:@"walk-left02"];
        SKTexture *wl3 = [walkl textureNamed:@"walk-left03"];
        SKTexture *wl4 = [walkl textureNamed:@"walk-left04"];
        SKTexture *wl5 = [walkl textureNamed:@"walk-left05"];
        SKTexture *wl6 = [walkl textureNamed:@"walk-left06"];
        SKTexture *wl7 = [walkl textureNamed:@"walk-left07"];
        SKTexture *wl8 = [walkl textureNamed:@"walk-left08"];
        SKTexture *wl9 = [walkl textureNamed:@"walk-left09"];
        NSArray *awl = @[wl1, wl2, wl3, wl4, wl5, wl6, wl7, wl8, wl9];
        SKAction *aw = [SKAction animateWithTextures:awl timePerFrame:0.2];
        
        //SpriteSheet *SS = [[SpriteSheet alloc] initWithTextureName:@"spritesheet-left-walk-2" rows:1 cols:9 margin:5 spacing:30];
        //SKSpriteNode *player = [SKSpriteNode spriteNodeWithTexture:[SS textureForColumn:0 andRow:0]];
        SKSpriteNode *player = [SKSpriteNode spriteNodeWithTexture:wl1];
        player.position = CGPointMake(_width/2, _height/2);
        [player runAction:[SKAction repeatActionForever:aw]];
        [self addChild:player];
    }
    
    return self;
}

- (UITextField *)createTextField
{
    UITextField *getCharName = [[UITextField alloc]
                                initWithFrame:CGRectMake(_width/2-50, (_height/4)+25, 200, 50)];
    getCharName.borderStyle = UITextBorderStyleRoundedRect;
    getCharName.textColor = [UIColor blackColor];
    getCharName.font = [UIFont systemFontOfSize:17.0];
    getCharName.placeholder = @"Enter your name here";
    getCharName.backgroundColor = [UIColor whiteColor];
    getCharName.autocorrectionType = UITextAutocorrectionTypeYes;
    getCharName.keyboardType = UIKeyboardTypeDefault;
    getCharName.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    return getCharName;
}
@end
