//
//  SpriteSheet.m
//  p05-ademola
//
//  Created by Lynda on 3/21/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

//Code from http://stackoverflow.com/questions/28008107/using-sprite-sheets-in-xcode
#import "SpriteSheet.h"

@implementation SpriteSheet

- (instancetype)initWithTextureName:(NSString *)name rows:(NSInteger)rows cols:(NSInteger)cols margin:(CGFloat)margin spacing:(CGFloat)spacing {
    SKTexture *texture = [SKTexture textureWithImageNamed:name];
    return [self initWithTexture:texture rows:rows cols:cols margin:margin spacing:spacing];
}

- (instancetype)initWithTexture:(SKTexture *)texture rows:(NSInteger)rows cols:(NSInteger)cols {
    return [self initWithTexture:texture rows:rows cols:cols margin:0 spacing:0];
}

- (instancetype)initWithTexture:(SKTexture *)texture rows:(NSInteger)rows cols:(NSInteger)cols margin:(CGFloat)margin spacing:(CGFloat)spacing {
    if (self == [super init]) {
        _texture = texture;
        _rows = rows;
        _cols = cols;
        _margin = margin;
        _spacing = spacing;
        _frameSize = [self frameSize];
        
    }
    return self;
}

- (CGSize)frameSize {
    
    CGSize newSize = CGSizeMake((self.texture.size.width - (self.margin * 2.0 + self.spacing * ((CGFloat)self.cols - 1.0))) / ((CGFloat)self.cols),
                                ((self.texture.size.height - ((self.margin * 2.0) + (self.spacing * ((CGFloat)self.rows - 1.0)))) / ((CGFloat)self.rows)));
    NSLog(@"");
    //NSLog(@"x: %f, y: %f", newSize.width, newSize.height);
    
    return newSize;
}

- (SKTexture *)textureForColumn:(NSInteger)column andRow:(NSInteger)row {
    
    if (column >= (self.cols) || row >= (self.rows)) {
        NSLog(@"Asking for row or col greater than spritesheet");
        return nil;
    }
    
    CGRect textureRect = CGRectMake(self.margin + (column * self.frameSize.height + self.spacing) - self.spacing,
                                    self.margin + (row * self.frameSize.width + self.spacing) - self.spacing, // note using width here
                                    self.frameSize.width,
                                    self.frameSize.height);
    
    textureRect = CGRectMake(textureRect.origin.x / self.texture.size.width, textureRect.origin.y / self.texture.size.height, textureRect.size.width / self.texture.size.width, textureRect.size.height/self.texture.size.height);
    
    return [SKTexture textureWithRect:textureRect inTexture:self.texture];
}
@end
