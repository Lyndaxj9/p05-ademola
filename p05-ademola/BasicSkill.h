//
//  BasicSkill.h
//  p05-ademola
//
//  Created by Lynda on 3/30/17.
//  Copyright © 2017 Lynda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Entity.h"
@class Entity;

@protocol BasicSkill <NSObject>
//- (void)execute;
- (void)execute:(Entity *)target;
@end
